﻿using Container.Utils;
using System;
using System.Collections.Generic;

namespace Link.Routing
{
	public interface ILinkRoutingHandler
	{
		bool ProcessUri(string url, Uri uri);
	}

	public class LinkRoutingManager : SingletonWithObservable<LinkRoutingManager, string, Action<object>?>
	{
		private class WrappedLogger
		{
			private Action<object>? Logger { get; }
			public WrappedLogger(Action<object>? logger)
			{
				this.Logger = logger;
			}

			public void Log(string message)
			{
				if (this.Logger != null)
				{
					this.Logger(message);
				}
			}
		}


		private class WrapHttpFuncAsHandler : ILinkRoutingHandler, IDisposable
		{
			private LinkRoutingManager Manager { get; }
			private Func<string, Uri, bool> Handler { get; }
			public WrapHttpFuncAsHandler(LinkRoutingManager manager, Func<string, Uri, bool> handler)
			{
				this.Manager = manager;
				this.Handler = handler;
			}

			public bool ProcessUri(string url, Uri uri)
			{
				return this.Handler(url, uri);
			}

			public void Dispose()
			{
				this.Manager.RemoveHttpHandler(this);
			}
		}

		private class WrapLinkHandler : ILinkRoutingHandler, IDisposable
		{
			private LinkRoutingManager Manager { get; }
			private string Path { get; }
			private Func<string, Uri, bool> Handler { get; }
			public WrapLinkHandler(LinkRoutingManager manager, string path, Func<string, Uri, bool> handler)
			{
				this.Manager = manager;
				this.Path = path;
				this.Handler = handler;
			}

			public bool ProcessUri(string url, Uri uri)
			{
				return this.Handler(url, uri);
			}

			public void Dispose()
			{
				this.Manager.RemoveLinkHander(this.Path);
			}
		}


		public string BundleIdentifier { get; }
		private WrappedLogger Logger { get; }

		private List<ILinkRoutingHandler> HttpHandlers = new List<ILinkRoutingHandler>();
		private Dictionary<string, ILinkRoutingHandler> Handlers = new Dictionary<string, ILinkRoutingHandler>();


		public LinkRoutingManager(string bundleIdentifier, Action<object>? logger = null)
		{
			this.BundleIdentifier = bundleIdentifier;
			this.Logger = new WrappedLogger(logger);
		}

		public IDisposable AddLinkHandler(string path, Func<string, Uri, bool> handler)
		{
			var wrapper = new WrapLinkHandler(this, path, handler);
			this.AddLinkHandler(path, wrapper);
			return wrapper;
		}

		public void AddLinkHandler(string path, ILinkRoutingHandler handler)
		{
			this.Handlers[path] = handler;
		}

		public bool RemoveLinkHander(string path)
		{
			return this.Handlers.Remove(path);
		}

		public IDisposable AddHttpHandler(Func<string, Uri, bool> handler)
		{
			var wrapper = new WrapHttpFuncAsHandler(this, handler);
			this.AddHttpHandler(wrapper);
			return wrapper;
		}

		public void AddHttpHandler(ILinkRoutingHandler handler)
		{
			this.RemoveHttpHandler(handler);
			this.HttpHandlers.Add(handler);
		}

		public bool RemoveHttpHandler(ILinkRoutingHandler handler)
		{
			return this.HttpHandlers.Remove(handler);
		}

		public bool ProcessForBundle(string bundleUrl)
		{
			return this.ProcessURL(string.Format("{0}://{1}", this.BundleIdentifier, bundleUrl));
		}

		public bool ProcessURL(string? url)
		{
			if (string.IsNullOrEmpty(url) == true)
			{
				return false;
			}

			if (Uri.TryCreate(url, UriKind.Absolute, out var uri) == true)
			{
				// Check to see if this scheme is http/s handler
				if ((uri.Scheme == "https") || (uri.Scheme == "http"))
				{
					// Check if the host is the bundle identfier
					if (uri.Host == this.BundleIdentifier)
					{
						var withoutBundle = url.Substring(url.IndexOf(this.BundleIdentifier) + this.BundleIdentifier.Length + 1);
						return this.ProcessForBundle(withoutBundle);
					}
					else
					{
						foreach (var handler in this.HttpHandlers)
						{
							var processed = handler.ProcessUri(url, uri);
							if (processed == true)
							{
								return true;
							}
						}
					}
				}
				else if (uri.Scheme == this.BundleIdentifier)
				{
					if (this.Handlers.TryGetValue(uri.Host, out var handler) == true)
					{
						return handler.ProcessUri(url, uri);

					}
					else
					{
						this.Logger.Log($"No handler registered for \"{uri.Host}\" from \"{url}\"");
					}
				}
			}
			else
			{
				this.Logger.Log("Unknown scheme/protocol \"" + url + "\"");
			}

			return false;
		}
	}
}
